// based on https://bitbucket.org/akudaikon/laundry-notify

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <Ticker.h>
#include <Button.h>
#include <HLW8012.h>

#include "config.h"

#define RELAY_PIN                       12
#define LED_PIN                         15
#define BUTTON_PIN                      0
#define SEL_PIN                         5
#define CF1_PIN                         13
#define CF_PIN                          14

// HLW8012 circuit parameters
#define CURRENT_RESISTOR                0.001
#define VOLTAGE_RESISTOR_UPSTREAM       (5 * 470000)
#define VOLTAGE_RESISTOR_DOWNSTREAM     1000

#define ON                              true
#define OFF                             false

#define UPDATE_RATE                     2000
#define ON_CURRENT_THRESHOLD            6.3
#define OFF_CURRENT_THRESHOLD           0.1
#define STATE_COUNTER_THRESHOLD         3

enum { STATE_OFF, STATE_ON, STATE_BREWING, STATE_KEEP_WARM };
const char* stateNames[4] = { "Off", "On", "Brewing", "Keep Warm" };
uint8_t state = STATE_OFF;

bool secure = false;
uint32_t lastUpdate = 0;
int stateCounter = 0;
char host[12];
char mqttTopic[18];

void relayOn() {
  digitalWrite(RELAY_PIN, ON);
  state = STATE_ON;
}

void relayOff() {
  digitalWrite(RELAY_PIN, OFF);
  state = STATE_OFF;
}

bool relayState() {
  return digitalRead(RELAY_PIN);
}

void toggleRelay() {
  relayState() ? relayOff() : relayOn();
}

// toggle the LED
void blink() {
  digitalWrite(LED_PIN, !digitalRead(LED_PIN));
}

HLW8012 hlw8012;
Ticker ticker;
WiFiManager wifiManager;
ESP8266WebServer http(80);
ESP8266HTTPUpdateServer httpUpdater;
WiFiClientSecure wifiClient;
Button btn(BUTTON_PIN, true, true, 100);

void unblockingDelay(unsigned long mseconds) {
  unsigned long timeout = millis();
  while ((millis() - timeout) < mseconds) delay(1);
}

void calibrate() {
  relayOn();

  // first read power, current, and voltage
  // with an interval to allow the signal to stabalize

  hlw8012.getActivePower();

  hlw8012.setMode(MODE_CURRENT);
  unblockingDelay(5000);
  hlw8012.getCurrent();

  hlw8012.setMode(MODE_VOLTAGE);
  unblockingDelay(5000);
  hlw8012.getVoltage();

  // calibrate using a 60W incandescent bulb (resistive load)
  hlw8012.expectedActivePower(60.0);
  hlw8012.expectedVoltage(119.0); // measured with kill-a-watt meter
  hlw8012.expectedCurrent(60.0 / 119.0);

  relayOff();
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  // handle incoming messages

  char payload_assembled[length];
  for (int i = 0; i < length; i++) {
    payload_assembled[i] = (char)payload[i];
  }

  if (strncmp(payload_assembled, "On", 2) == 0) {
    relayOn();
  }

  if (strncmp(payload_assembled, "Off", 3) == 0) {
    relayOff();
  }

}

PubSubClient mqtt(MQTT_HOST, MQTT_PORT, mqttCallback, wifiClient);

void hlw8012_cf1_interrupt() { hlw8012.cf1_interrupt(); }
void hlw8012_cf_interrupt() { hlw8012.cf_interrupt(); }

// gets called when WiFiManager enters config mode
void configModeCallback(WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  Serial.println(myWiFiManager->getConfigPortalSSID());
  // entered config mode; LED blink faster
  ticker.attach_ms(200, blink);
}

String stateToJSON() {
  String response = "{";
  response += "\"relay\":";
  response += (relayState() ? "true" : "false");
  response += "}";
  return response;
}

void getState() {
  // don't cache this response
  http.sendHeader("Cache-Control", "max-age=0");
  http.send(200, "application/json", stateToJSON());
}

void getRelay() {
  http.send(200, "text/plain", (relayState() ? "true" : "false"));
}

void postRelay() {
  // x-www-form-urlencoded key:relay value: true/false/toggle
  if (http.arg("relay") == "true") return relayOn();
  if (http.arg("relay") == "false") return relayOff();
  if (http.arg("relay") == "toggle") return toggleRelay();
}

String powerToJSON() {
  String response = "{";

  response += "\"watts\":\"";
  response += hlw8012.getActivePower();
  response += "\",\"amps\":\"";
  response += hlw8012.getCurrent();
  response += "\",\"volts\":\"";
  response += hlw8012.getVoltage();
  response += "\"}";

  return response;
}

void getPower() {
  // don't cache this response
  http.sendHeader("Cache-Control", "max-age=0");
  http.send(200, "application/json", powerToJSON());
}

void setup() {
  // init serial
  Serial.begin(115200);

  // Turn off relay
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, OFF);

  // Turn off LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, OFF);

  // start blinking the LED
  ticker.attach_ms(600, blink);

  // get MAC Address
  uint8_t mac[6];
  WiFi.macAddress(mac);

  // make hostname
  sprintf(host, "%s%02x%02x%02x", "pow-", mac[3], mac[4], mac[5]);
  Serial.println(host);

  // make mqtt topic
  sprintf(mqttTopic, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], mac[6]);
  Serial.print("mqtt topic: ");
  Serial.println(mqttTopic);

  // set config mode callback
  wifiManager.setAPCallback(configModeCallback);

  // fetch ssid and pass and try to connect
  // or go into blocking AP captive portal mode
  if (!wifiManager.autoConnect(host, "configure")) {
    Serial.println("failed to connect and hit timeout");
    // reset and try again
    ESP.reset();
  }

  // if you're here; you've connected to the wifi!
  Serial.println("connected... yeey! :)");

  // start mDNS
  if (MDNS.begin(host)) {
    Serial.println();
    Serial.print("Hostname: ");
    Serial.println(host);
  }

  // stop blinking LED and turn it off
  ticker.detach();
  digitalWrite(LED_PIN, OFF);

  // Init HLW8012
  // interrupts mode true. if false, set pulse_timeout
  // pulse_timeout 500000 = 24w precision, 100000 = 12w, 200000 = 6w?
  hlw8012.begin(CF_PIN, CF1_PIN, SEL_PIN, HIGH, true);
  hlw8012.setResistors(CURRENT_RESISTOR, VOLTAGE_RESISTOR_UPSTREAM, VOLTAGE_RESISTOR_DOWNSTREAM);
  attachInterrupt(CF1_PIN, hlw8012_cf1_interrupt, CHANGE);
  attachInterrupt(CF_PIN, hlw8012_cf_interrupt, CHANGE);

  // connect to mqtt broker
  wifiClient.connect(MQTT_HOST, MQTT_PORT);

  // verify ssl fingerprint
  if (wifiClient.verify(MQTT_SHA1, MQTT_HOST)) {
    Serial.println("mqtt connection secure!");
    secure = true;
  } else {
    Serial.println("CONNECTION NOT SECURE");
    // TODO prevent anything else from happening
    secure = false;

    // blink to notify of a problem
    ticker.attach_ms(100, blink);
  }

  // stop wifi client
  wifiClient.stop();

  // HTTP API
  http.on("/state", HTTP_GET, getState);
  http.on("/relay", HTTP_GET, getRelay);
  http.on("/relay", HTTP_POST, []() {
    // set the relay
    postRelay();
    // send http response
    getRelay();
    // publish state to mqtt
    mqtt.publish(mqttTopic, (relayState() ? "On" : "Off"));
  });

  http.on("/power", HTTP_GET, getPower);

  http.on("/restart", HTTP_GET, []() { http.send(200); ESP.restart(); });
  http.on("/reset", HTTP_POST, []() { http.send(200); ESP.reset(); });

  http.onNotFound([]() { http.send(404, "text/plain", "Not Found");  });

  // Start http server
  httpUpdater.setup(&http);
  http.begin();

  // calibrate if needed
  // calibrate();
}

void reconnect() {
  if (!mqtt.connected()) {
    Serial.println("Attempting MQTT connection...");
    // attempt to connect
    if (mqtt.connect(mqttTopic, MQTT_USER, MQTT_PASS, mqttTopic, 0, false, "Offline")) {
      Serial.println("connected");
      // once connected, publish state
      //client.publish(mqttTopic, stateNames[state], true);
      mqtt.publish(mqttTopic, "I'm a coffee pot ☕️", false);
      // subscribe to topics
      mqtt.subscribe(mqttTopic);
      mqtt.loop(); // needed to make the subscribe happen
    } else {
      Serial.print("failed, rc=");
      Serial.println(mqtt.state());
      // try again next loop
    }
  }
}

void advanceState (bool condition) {
  // track how long the condition is true
  if (condition) {
    if (stateCounter > STATE_COUNTER_THRESHOLD) {
      // go to the next state
      state++;
      // tell the world
      mqtt.publish(mqttTopic, stateNames[state]);
    } else {
      stateCounter++;
    }
  }
}

void loop() {
  // only do stuff if secure mqtt connection has been established
  if (secure) {
    if (!mqtt.connected()) {
      reconnect();
    }

    // Handle MQTT messages
    mqtt.loop();

    // Handle HTTP server requests
    http.handleClient();

    // Handle button press
    btn.read();
    if (btn.wasPressed()) {
      // toggle relay
      toggleRelay();
      // publish on MQTT
      mqtt.publish(mqttTopic, (relayState() ? "On" : "Off"));
    } else if (btn.pressedFor(5000)) {
      Serial.println("**** RESET ****");
      wifiManager.resetSettings();
      // turn on the LED
      digitalWrite(LED_PIN, ON);
      mqtt.publish(mqttTopic, "**** RESET ****");
      delay(3000);
      ESP.restart();
    }

  // wait for a few samples
  if ((millis() - lastUpdate) > UPDATE_RATE) {
    // update measurement time
    lastUpdate = millis();

    // needed for getCurrent to work correctly
    hlw8012.getActivePower();

    switch (state) {
      case STATE_OFF:
      case STATE_KEEP_WARM:
        // reset state counter
        stateCounter = 0;
        break;
      case STATE_ON:
        advanceState(hlw8012.getCurrent() > ON_CURRENT_THRESHOLD);
        break;
      case STATE_BREWING:
        advanceState(hlw8012.getCurrent() < OFF_CURRENT_THRESHOLD);
        break;
       }

      /* continuously publish power reading
        String response;
        response += hlw8012.getActivePower();
        response += "W, ";
        response += hlw8012.getCurrent();
        response += "A, ";
        response += hlw8012.getVoltage();
        response += "V";
        Serial.println(response);
        char buff[50];
        response.toCharArray(buff, 50);
        mqtt.publish(mqttTopic, buff, false);
      */
    }
  }
}
